# rhodopsin-results


## Installation
There is NO INSTALLATION required for the code.

For Windows -  download and install gitbash found here
https://gitforwindows.org/


We recommend installing Anaconda: https://docs.continuum.io/anaconda/install/
The Anaconda installation includes python, numpy, scipy, pandas, and matplotlib. 

To view hdf5 files please install h5py by typing
"conda install h5py"
in your terminal/anaconda powershell prompt (on windows)

cd into your folder where you want to clone the repo 
(for Windows: open "Windows PowerShell" and then cd into folder)

git clone https://gitlab.com/kirianlab/rhodopsin-results.git

After which repository folders will appear in the directory



## Folder Structure

The "rhodopsin-results" folder contains three subfolders 
1. "Scattering_profiles" 
2. "Electron_density"
3. "Time_tool"

This folder only contains scripts.

Data corresponding to the above folders are available to  download at: https://www.dropbox.com/scl/fo/udjj4thbnlw3ck0v3eng9/AGDfk1wNf5kyywYX7hsJHdg?rlkey=134o52fpw31jntc9v20yipjfk&dl=0

place the data into their corresponding folder on your local repo


### 1. Scattering_profiles folder
"Scattering_profiles" contains bash scripts, python scripts and Jupyter notebooks which were used for data reduction and analysis of scattering profiles. It also has Examples directory with the data for one experiment and corresponding python notebooks 

"Scattering_profiles/radpro_mean_maxmasked.py" was used at the beamline
for making scattering profiles from experimental runs 

Scattering_profiles contains three Jupyter notebooks which are as follows:
1. "saxs.opsin.smartsubdarks.ipynb" - for light minus dark opsin scattering profiles are produced 
2. "saxs.rhods.smartsubdarks.ipynb" - for light minus dark 
rhodopsin scattering profiles are produced 
3. "saxs.smartsubdarks.smartsubdoublediff.ipynb - for rhodopsin minus opsin scattering  profiles 

"Scattering_profiles/results" is the directory where the figures related to the X-ray scattering are saved the python notebook#3 is executed 

The above Dropbox folder currently has all the data to execute the jupyter-notebook #3




### 2. Electron_density folder 

contains "edensity_sum_standalone_submitted.py"  which creates Figures 3c center and Fig SI 9 center

Also has "resources" containing scripts called in the "edensity_sum_standalone_submitted.py"

the averaged electron density files are available in the corresponding folder 

### 3. Time_tool folder

has scripts used at the beamline and run at PSANA system as SLAC National Laboratory.





## Usage

To load Jupyter notebooks:
1. cd into the folder using the terminal /(Anaconda powershell prompt) where the notebooks are located

2. type "jupyter notebook notebook_name.ipynb"
3. it will open the notebook in your default website

For more instructions: https://docs.jupyter.org/en/latest/running.html

To execute the python script:
python script_name.py

Detailed comments are provided inside the respective scripts and jupyter notebooks


## Support
For any issues contact csmenon@arizona.edu




## Authors and acknowledgment
Thomas D. Grant
Richard A. Kirian
C. Swathi K. Menon 

## License
GNU Open Source License

