********************
README
********************

The bash scripts were run at PSANA system at SLAC National Laboratories to produce average scattering profiles as HDF5 files 
For "Dark" -Optical laser off- Rhodopsin and Opsin experimental runs
For "Pump" -Optical Laser On - Rhodopsin and Opsin experimental runs

in the file name "rhods" indicates rhodopsin and "pump" and "dark" have above meanings

For more information:
https://confluence.slac.stanford.edu/display/PCDS/Running+on+psana
