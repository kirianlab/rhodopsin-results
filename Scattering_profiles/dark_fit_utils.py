"""
Utilities for fitting radial profiles
"""

import numpy as np
from scipy import optimize
import sys, datetime

def opt_func(coeffs, vecs, pumped, minrbin, maxrbin):
    """
    coeffs, tuple of parameters
    vecs, the dark sigular vectors [ U[:,0]*s[0], U[:,1]*s[1], ... ]
    pumped, a pumped radial profile, could be average or individual shot

    returns the sum of squared residuals
    """
    linear_comb_of_vecs = _get_linear_comb(coeffs, vecs)
    resid = pumped - linear_comb_of_vecs
    ressum = np.sum(resid[minrbin:maxrbin] ** 2)
    return ressum


def _get_linear_comb(coeffs, vecs):
    """
    Returns a linear combination of vectors based on params.

    coefs must be N+1 long where N is the length
    of vecs. The extra coef (at the end of the list)
    is a constant offset to the fit

    coeffs, same linear coeffs passed to opt_func
    vecs, same dark_vecs passed to opt_func
    """
    linear_comb = np.zeros_like(vecs[0])
    for v, c in zip(vecs, coeffs[:-1]):
        linear_comb += v * c
    linear_comb += coeffs[-1]
    return linear_comb


def fit_darks_to_pumps(df_pumped,
                       x0,
                       vecs,
                       minrbin=0,
                       maxrbin=-1,
                       method='Nelder-Mead',
                       fun=opt_func, on=None):
    """
    Loop over pumped profiles , fitting a dark profile to each one

    returns a results dictionary with many useful parameters,
    in perticular the optimized difference profile per shot

    The lists in the dictionary values are aligned to the dataframe indices
    """
    if on is None:
        on = "radials"
    results = {}
    results['dark_fits'] = []
    results['differences'] = []
    results['success'] = []
    results['residuals'] = []

    for i_pumped, pumped in enumerate(df_pumped[on]):

        if i_pumped % 50 == 0:
            sys.stdout.flush()
            print("\rPumped %d/ %d" % (i_pumped + 1, len(df_pumped))) #,
        #print "pre-fit: ", datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
        dark_fit, fit = _get_dark_fit(pumped, x0, vecs, minrbin, maxrbin, method, fun)
        #print "post-fit: ", datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")

        results['dark_fits'].append(dark_fit)
        results['residuals'].append(fit['fun'])
        results['success'].append(fit['success'])

        results['differences'].append(pumped - dark_fit)

    print("Done!\n")
    return results


def _get_dark_fit(pumped_profile, x0, vecs, minrbin=0, maxrbin=-1, method='Nelder-Mead', fun=opt_func):
    """
    fit vector combination to a pumped profile

    returns tuple of (fitted_profile, fit_results)

    fit_results is the output of the scipy optimize method
    """


    fit = optimize.minimize(fun=fun,
                            x0=x0,
                            args=(vecs, pumped_profile, minrbin, maxrbin),
                            method=method,
                            options={'maxiter': 50000, 'maxfev': 50000})

    dark_fit = _get_linear_comb(fit['x'], vecs)

    return dark_fit, fit

def _get_dark_fit_lstsq(pumped_profile, vecs, xmin=0, xmax=-1):
    """
    fit vector combination to a pumped profile using least squares

    returns tuple of (fitted_profile, coefficients)
    """
    vecs = np.array(vecs)
    a = vecs.T
    b = pumped_profile
    x, resid, rank, s = np.linalg.lstsq(a[xmin:xmax],b[xmin:xmax],rcond=None)
    dark_fit = _get_linear_comb(x, vecs)

    return dark_fit, x





