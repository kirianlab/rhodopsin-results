import psana
import sys
import numpy as np
import time
import argparse
from scipy import ndimage, stats
from scipy.sparse.linalg import svds
import matplotlib.pyplot as plt
import pyqtgraph as pg
sys.path.append("/reg/d/psdm/cxi/cxilu1817/scratch/tgrant/analysis/cxilu1817/rick/")  # update this, or set PYTHONPATH
from svd_utils import addblock_svd_update
sys.path.append("/reg/d/psdm/cxi/cxilu1817/scratch/tgrant/analysis/")
from cxilu1817 import dark_fit_utils_new
from cxilu1817 import stats_new

parser = argparse.ArgumentParser(description='Average many patterns!')

parser.add_argument('-r', dest='run', type=int, required=True, 
    help='run number to process')

parser.add_argument('--mpi', dest='mpi', action="store_true",
    help='whether to use mpi datasource')

parser.add_argument('--plot', dest='plot', action="store_true",
    help='plot streaks; disables mpi and data storage')

parser.add_argument('--saxs', dest='saxs', action="store_true",
    help='mark the run as saxs')

parser.add_argument('--detector-string', dest='det_str', type=str, 
    default="DscCsPad", 
    help='run number to process')

parser.add_argument('--exp-string', dest='exp_str', type=str, 
    default="cxilu1817", 
    help='experiment string')

parser.add_argument('-N', dest='Nevents',type=int, default=None, 
    help='Number of events to process')

parser.add_argument('-o', dest='outname',type=str, required=True, 
    help='output filename (*.npy)')

parser.add_argument('--include-codes', dest='incl_codes', nargs='*', 
    type=int, default=[], 
    help='only process events with these codes')

parser.add_argument('--exclude-codes', dest='excl_codes', nargs='*', 
    type=int, default=[], 
    help='do not process events with these codes')

parser.add_argument('--energy-min', dest='ener_min', type=float,
    default=0, help='MINimum photon energy in eV')

parser.add_argument('--energy-max', dest='ener_max', 
    type=float, default=np.inf, 
    help='MAXimum photon energy in eV')

parser.add_argument('--mask-file', dest='mask_fname', 
    type=str, default='data_files/waxs.maskerpiece.npy', 
    help='Path to a mask(s) .npy file name (shape 32x185x388; False means masked)')

parser.add_argument('--R-file', dest='R_fname',
    type=str, default='data_files/panelR.npy', 
    help='Path to a mask .npy file name (shape 32x185x388) containing the radial coordinate values of each pixel (distance from center in pixel units)')

parser.add_argument('--gain-file', dest='gain_fname', 
    type=str, default=None,  
    help='Path to a gain .npy file name (shape 32x185x388)')

parser.add_argument("--matcher-array", dest="matcher", default='data_files/matcher_panel1.npy', type=str, help="path to matched array npy file, shape of array is a single panel (185 x 388) ")
parser.add_argument("--matcher-idx", dest="matcher_idx", default=1, type = int, help="index of matched panel array (number between 0 and 31)")
parser.add_argument("--matcher-cutoff", dest="matcher_cutoff", type=float,help="cutoff value for matcher comparison, the comparison is the (matcher * panel).sum()")

args = parser.parse_args()

MATCHER = np.load(args.matcher)

tstart = time.time()

if args.mask_fname is None:
    BASE_MASK = np.ones( (32,185,388) ).astype(np.bool)
else:
    BASE_MASK = np.load( args.mask_fname)
    assert( BASE_MASK.shape == (32,185,388))

# load the R data (required!)
R = np.load(args.R_fname) 
R1 = R.ravel().astype(np.int) # bin-labels (bin size is 1 radial pixel) 
minlength= R1.max() + 10 # number of radial bins
R13d = R1.reshape(R.shape)

# count total number of events
ds_idx = psana.DataSource("exp=%s:run=%d:idx"%(args.exp_str,args.run))
max_events = len( ds_idx.runs().next().times() )
# check if user wants all events or a smaller number of events
if args.Nevents is None:
    Nevents = max_events 
else:
    Nevents = min( args.Nevents, max_events)
#print("\nI will process %d events"% Nevents)
del ds_idx

if args.plot:
    import pylab as plt
    fig = plt.figure(figsize=(4,4))
    ax = plt.gca()

mpi = args.mpi
if args.plot and mpi:
    mpi = False

# load the Datasource
if mpi:
    ds = psana.MPIDataSource("exp=%s:run=%d:smd"%(args.exp_str,args.run))
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
else:
    ds = psana.DataSource("exp=%s:run=%d:smd"%(args.exp_str,args.run))
events = ds.events()

# get the detector names for experiment/run
detnames = [ d for sl in psana.DetNames() for d in sl]
#print "\nI found these detectors"
#print "\n\t".join([d for d in detnames if d != ""])
print

# check if a gain was provided
if args.gain_fname is not None:
    gain = np.load(args.gain_fname)
    gain[ gain==0] = 1 # prevent any 0-division... just in case...
else:
    gain = np.ones( (32,185,388) )


# load the main cspad detector
assert( args.det_str in detnames)
cspad = psana.Detector(args.det_str)
ebeam = psana.Detector('EBeam') # photon energy data for filtering

# evr codes for laser logic
env = ds.env()
code_dets = [ psana.Detector(d, env) 
    for d in detnames if d.startswith("evr") ]

# Intensity thresholds for filtering, we need to keep 
#  about 1-photons-worth of negative pixels for proper statistics
minI,maxI = -140, 16300
#for SAXS, mask bright pixels from jet streak
#25 * max photons (since ADU/photon = 25)
#maxI = 25*15
#do we need max photons per rbin?
#look at 2D histograms to decide
#looks like 6 photons is maximum for most bins
#except the lower res bins less than bin ~150
#low res bins gradually get brighter towards the 
#center, so need to change those bins specifically
#by hand based on the 2D histogram
rbins, npixperrbin = np.unique(R1,return_counts=True)
nrbins = len(rbins)
maxList = np.zeros(nrbins)
grouped_rbins = []
if args.saxs:
    saxs = True
else:
    saxs = False
if saxs:
    grouped_rbins.append(range(0,30))
    grouped_rbins.append(range(30,50))
    grouped_rbins.append(range(50,70))
    grouped_rbins.append(range(70,90))
    grouped_rbins.append(range(90,150))
    grouped_rbins.append(range(150,nrbins))
    maxList[grouped_rbins[0]] = 13
    maxList[grouped_rbins[1]] = 11
    maxList[grouped_rbins[2]] = 9
    maxList[grouped_rbins[3]] = 8
    maxList[grouped_rbins[4]] = 7
    maxList[grouped_rbins[5]] = 6
else:
    maxList = np.ones(nrbins)*np.inf

#for raw histograms
make_2dhist = True

if saxs:
    MATCHER = np.load('data_files/saxsmatcher_panel1.npy')
    MATCHER2 = np.load('data_files/saxsmatcher2_panel1.npy')
    matcher_cutoff = 10.0
else:
    MATCHER = np.load('data_files/waxsmatcher_panel1.npy')
    MATCHER2 = np.load('data_files/waxsmatcher2_panel1.npy')
    matcher_cutoff = 3.0

# small data  handler
if mpi:
    smldata = ds.small_data(args.outname, gather_interval=100)

non_masked_counts = np.zeros_like( BASE_MASK).astype(np.float32)

partial_panel_sum = None
partial_panel_sum_unmasked = None
seen_events = 0
processed_events = 0

im1d = []

radials = []

#Nevents = 500
print Nevents

for_hist = []
imint1d = None
phperpix = []
list_nphotons = []
#select the high resolution part of the image
if seen_events <= 1:
    selmask = np.ones(R13d.shape,dtype=np.bool)
    selmask[R13d<300] = False
    selmask[R13d>1000] = False

try:
    for i_ev in range( Nevents):
        if mpi:
            if i_ev%size!=rank: 
                continue
        ev = events.next()

        #just for testing run 153, only look at events towards the end
        #when moving the detector back to the SAXS region
        #if i_ev < 20000: continue
        
        if ev is None:
            print "%i i_ev is None" % i_ev
            continue

#       Check the event codes (e.g. xrays on or not, pump laser on)
        codes = []
        for cd in code_dets:
            c = cd.eventCodes(ev)
            if c is not None:
                codes += c
        
        if any ( [ c in codes for c in args.excl_codes]):
            #print "excluded"
            continue
        if not all ([ c in codes for c in args.incl_codes]):
            #print "not included "
            continue

#       ===========================================
#       Photon energy bandpass filter
        eb = ebeam.get(ev)
        if eb is None:
            continue
        
        ener = eb.ebeamPhotonEnergy()
        if ener < args.ener_min:
            continue
        if ener > args.ener_max:
            continue       

#       Process the image
        panel = cspad.calib( ev, cmprs=(5,0,0,0))
        #panel = cspad.raw( ev)
        if panel is None:
            continue

        seen_events += 1

        MASK = BASE_MASK.copy() 
        #mask some more of the jet
        MASK[1,:,:30] = 0
        MASK[17,:,25:75] = 0
        #and some more
        MASK[0,:,:30] = 0
        MASK[16,:,25:75] = 0
        
        if mpi:
            if partial_panel_sum_unmasked is None:
                partial_panel_sum_unmasked = panel
            else:
                partial_panel_sum_unmasked += panel

        #the matcher panel is used to discriminate between a user-deemed 
        #good / bad shot, based on the supplied matcher array
        #first convert to photons
        matcher_panel = np.rint(panel[ args.matcher_idx]/25)
        #then calculate the average of the intensity in each region
        matcher_sorted = np.sort(matcher_panel[MATCHER==1])
        matcher_avg = np.sum(matcher_panel*MATCHER)/MATCHER.sum()
        matcher2_avg = np.sum(matcher_panel*MATCHER2)/MATCHER2.sum()
        #calculate the ratio of the average intensity of the jet streak to the image
        matcher_ratio = matcher_avg / matcher2_avg
        if not np.isfinite(matcher_ratio):
            matcher_ratio = 0.0
        if matcher_ratio < matcher_cutoff:
            print("Event %d is not a good match. Matcher ratio: %f"%(i_ev,matcher_ratio))
            #continue
        else:
            print("Event %d is a good match. Matcher ratio: %f"%(i_ev,matcher_ratio))
        
        """
        tmp = matcher_panel + matcher_panel*MATCHER2*10
        pg.image(tmp[:,:])
        pg.QtGui.QApplication.instance().exec_()
        """

        

        panel = panel*MASK/gain 
        where_bad_intens = np.logical_or( panel < minI, panel> maxI)
        panel[ where_bad_intens] = 0
        MASK[ where_bad_intens] = False
        
        non_masked_counts += MASK

        if mpi:
            if partial_panel_sum is None:
                partial_panel_sum = panel
            else:
                partial_panel_sum += panel



        im3d = panel*MASK
        im1d = np.copy(im3d).ravel()

        
        if imint1d is None:
            #convert to photon counts as integers
            #photon per ADU = 25
            im1d[im1d<20] = 0
            im1d /= 25
            #round to the nearest integer (note np.rint, not np.int)
            imint1d = np.rint(im1d)
            imint3d = imint1d.reshape(im3d.shape)
            phobins = np.arange(40) #photon bins
            histlist = [None]*nrbins
            #mask out bright pixels from jet streak
            #based on photon counts
            maxMASK = np.ones(im3d.shape,dtype=np.bool)
            if saxs:
                #looping over each bin is too slow
                #since most of the bins have the same max value
                #just loop over the few that are designated as different
                for group in grouped_rbins:
                    #just take the max as the max of the first rbin in group
                    maxvalue = maxList[group[0]]
                    maxMASK[np.in1d(R13d,group).reshape(imint3d.shape)&(imint3d>maxvalue)] = False
            imint1d *= maxMASK.ravel()
            imint3d *= maxMASK
            #for histogram, need to account for different numbers of pixels in each bin
            #using weighting, just need to calculate this once
            weights = np.ones(R13d.shape)
            for rbin in range(nrbins):
                if npixperrbin[rbin] != 0:
                    weights[R13d==rbin] = 1./npixperrbin[rbin]
            #need to mask histogram using weights
            newMASK = MASK*maxMASK
            #count the total number of photons per image
            #just look at the region in selmask
            nphotons = np.sum(imint3d[newMASK*selmask])
            n = 1
            if make_2dhist:
                newweights = np.copy(weights)*newMASK
                histlist, phobins, rbins = np.histogram2d(imint1d,R1,bins=[phobins,rbins],weights=newweights.ravel())
        else:
            im1d[im1d<20] = 0
            im1d /= 25
            imint1d = np.rint(im1d)
            imint3d = imint1d.reshape(im3d.shape)
            maxMASK = np.ones(im3d.shape,dtype=np.bool)
            if saxs:
                for group in grouped_rbins:
                    #just take the max as the max of the first rbin in group
                    maxvalue = maxList[group[0]]
                    maxMASK[np.in1d(R13d,group).reshape(imint3d.shape)&(imint3d>maxvalue)] = False
            imint1d *= maxMASK.ravel()
            imint3d *= maxMASK
            #for histogram, need to account for different numbers of pixels in each bin
            #need to mask histogram using weights
            #and update for each individual image
            #but original weights should stay the same, just mask out
            #pixels that should be ignored
            newMASK = MASK*maxMASK
            nphotons = np.sum(imint3d[newMASK*selmask])
            n += 1
            if make_2dhist:
                newweights = np.copy(weights)*newMASK
                histlist += np.histogram2d(imint1d,R1,bins=[phobins,rbins],weights=newweights.ravel())[0]

        #exclude weak patterns, i.e. less than 100,000 photons in selmask (~1 photon/10 pixels)
        if nphotons < 100000:
            continue

        if i_ev % 100 == 0: 
            print i_ev, nphotons

        radpro = ndimage.labeled_comprehension(
            input=imint3d[newMASK].ravel(),
            labels=R1[newMASK.ravel()], 
            index=np.arange(0,R1.max()+1),
            func=np.mean,
            out_dtype=np.float,
            default=0
            )
    
        if mpi: 
            #smldata.event() by default also saves timestamps, fiducials and more
            smldata.event( 
                radialpro = radpro,
                nphotons_in_selmask = nphotons,
                i_event = i_ev,
                )
           
        tnow = time.time()
        t_per_ev =seen_events / ( tnow - tstart)
        #print \
        #"\r Successfully processed %d / %d events (%.4f event per sec);" \
        #    %(processed_events+1, seen_events,  t_per_ev),
        processed_events += 1
except StopIteration:
    pass

if mpi and processed_events > 0:
    #panel_sum = smldata.sum( partial_panel_sum)
    #panel_sum_unmasked = smldata.sum( partial_panel_sum_unmasked)
    #smldata.save(panel_sum=panel_sum)
    #smldata.save(panel_sum_unmasked=panel_sum_unmasked)
    #smldata.save(base_mask=BASE_MASK)
    #smldata.save(counts=non_masked_counts)
    smldata.save(mask=MASK)
    smldata.save(selmask=selmask)
    if make_2dhist:
        new2dhistlist = np.empty_like(histlist)
        comm.Reduce(np.ascontiguousarray(histlist),new2dhistlist)
        if rank == 0: new2dhistlist = new2dhistlist.T
        smldata.save(histogram_2d=histlist)
        smldata.save(photon_bins=phobins)
        smldata.save(radial_bins=rbins)









