import psana
import argparse
import h5py
import numpy as np
import time
import sys
import matplotlib.pyplot as plt
from time_tool import timeToolAnalyzer

parser = argparse.ArgumentParser(description='Data grab')
#parser.add_argument('--reference', type=str, required=True, help='path to reference file')
args = parser.parse_args()
write = sys.stdout.write
run= 31 #Timetool Calibration 
exp= "cxilu1817"
s0 = 33.465 # millimeter, t0 stage value
#ref=np.load(args.reference)['ref']
#ds = psana.DataSource("exp=%s:run=%s:smd"%(exp,run))
ds = psana.DataSource(f"exp={exp}:run={run}:smd")
#ds = psana.DataSource(f"exp=cxilu1817:run=31:smd")

env = ds.env()
# get the detector names for experiment/run
detnames = [ d for sl in psana.DetNames() for d in sl]

#detectors
time_dial_det = psana.Detector( "LAS:FS5:VIT:FS_TGT_TIME_DIAL", ds.env() )
stage_pos = psana.Detector( "CXI:LAS:MMN:04.RBV", ds.env())
trace_det = psana.Detector('Timetool')
gas = psana.Detector("FEEGasDetEnergy")
gas_min = 0.5
events = ds.events()
show_jitter_hist =  False


# Evr detector objects for laser on/off event codes 
evr_dets = [ psana.Detector(d, env) for d in detnames if d.startswith("evr") ]
pump_on = 183 #evr code for Laser On
#event_ids =[] 
#for i, event in enumerate(events):
#    event_id = event.get(psana.EventId)
#    event_ids.append((event_id.time()[0], event_id.time()[1], event_id.fiducials()))
#    print(i)

#n_events = len(event_ids)

#edgepos = np.zeros(n_events, dtype=np.float32)
#target_del = np.zeros(n_events, dtype=np.float32)
edgepos = []
target_del = []
print("here")

TT= timeToolAnalyzer([1, 1, 1], kernelWidth=50)

def func(x, a, b, c):
    return a + b * x + c * x**2

for i, ev in enumerate(events):
    #print("here2")
    #if i > 10: 
    #    break

#   parse all event codes for pump on and off
    codes = []
    for det in evr_dets:
        c = det.eventCodes(ev)
        if c is not None:
            codes += c
    print(i,' \r')

    if pump_on in codes:
        is_pump = True
    else:
        is_pump = False

    #gas_en = gas.get(ev)
    #if gas_en is None:
        #write('gas_en is None\n')
        #continue

    #gas_en = gas_en.f_11_ENRC()

    #if gas_en <= gas_min:
     #   is_ref = True
    #else:
     #   is_ref = False

    if pump_on in codes:
        is_pump = True
    
    if is_pump is True:

        trace = trace_det.raw_data(ev)
        if trace is None:
            write('trace is None\n')
            continue
        
        lineout =trace[15:65].mean(0)
        result = TT.analyze(lineout)
        if result[0]['maxPix'] > 200 and result[0]['maxPix'] < 850: # just rejecting all the edgeposition below 200 pixels 
            edgepos.append( result[0]['maxPix']) 
            target_del.append( time_dial_det(ev))

print(np.shape(edgepos), np.shape(target_del))
edgepos= np.array(edgepos)


target_del = np.array(target_del)
fit =  np.polynomial.polynomial.Polynomial.fit(edgepos, target_del, 2, full=False)
#fitline = fit[0] + fit[1] * edgepos + fit[2] * edgepos**2 
coeff = fit.convert().coef
fitline = coeff[0] + coeff[1] * edgepos + coeff[2] * edgepos**2
print(fitline, np.shape(fitline))
fig, ax = plt.subplots();ax.set_box_aspect(0.75)
ax.plot(edgepos, target_del, '.')

ax.plot(edgepos,fitline,'.', label= "polynomial fit")
ax.set_xlabel("Edge Position/Pixels")
ax.set_ylabel("Target Time Delay/ps")
ax.legend()

#coeff = fit.convert().coef
print(coeff)
ax.text(810, -0.00075, f"Fit parameters: {coeff[0]} \n {coeff[1]} \n {coeff[2]}", ha = 'right', va = 'bottom')
plt.savefig("calibrationv4.svg", dpi=1200)
plt.show() 
plt.savefig("calib.svg", dpi=1200)




     
