###########################
#######
import psana
import argparse
import h5py
import numpy as np
import time
import sys
import matplotlib.pyplot as plt
from time_tool import timeToolAnalyzer
#from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Arial']})
#rc('text')


parser = argparse.ArgumentParser(description='Data grab')
parser.add_argument('-r', '--run', type=int, required=True, help='run number to process')
parser.add_argument('-f', '--fname', type=str, default=None, help='output basename')
parser.add_argument('--reference', type=str, required=True, help='path to reference file')
parser.add_argument('--exp-string', dest='exp',type=str, default="cxilu1817", help="experiment string")
parser.add_argument('-v', dest='v', action='store_true', help="verbose output")
parser.add_argument('--display-plots', dest='disp', action='store_true', help='Display some plots to show whats going on')

args = parser.parse_args()
tstart = time.time()
# experiment run number

exp = args.exp

ref = np.load(args.reference, allow_pickle=True)['ref']  # starting reference, references were few and far in between in this experiment
                                      # (once per 10,000 events hence why we use fast filter to try and gran more)

if args.v is True:
    write = sys.stdout.write
else:
    def write(msg):
        pass


TT = timeToolAnalyzer([1, 1, 1], kernelWidth=50, visualize=args.disp)

# evr codes
pump_on = 183
gas_min = 0.5
# 
ds = psana.MPIDataSource("exp=%s:run=%d:smd"%(exp,args.run))
env = ds.env()
gas = psana.Detector("FEEGasDetEnergy")
# get the detector names for experiment/run
detnames = [ d for sl in psana.DetNames() for d in sl]

smldata = ds.small_data(args.fname ,gather_interval=100)

# evr codes for laser logic
code_dets = [ psana.Detector(d, env) 
    for d in detnames if d.startswith("evr") ]

#trace
time_dial_det = psana.Detector( "LAS:FS5:VIT:FS_TGT_TIME_DIAL", ds.env() )
stage_pos = psana.Detector( "CXI:LAS:MMN:04.RBV", ds.env())
trace_det = psana.Detector('Timetool')

# Swathi Assumes these are time-tool calibration fit parameters 
pa,pb,pc = -1.004, 0.0025587,0

events = ds.events()
write('Analyzing events...\n')
s0 = 33.465 # millimeter, t0 stage value
for i_ev, ev in enumerate( events) :

    write('Event %d ' % (i_ev))
    if ev is None:
        write('ev is None\n')
        continue


#   parse all event codes
    codes = []
    for det in code_dets:
        c = det.eventCodes(ev)
        if c is not None:
            codes += c
    gas_en = gas.get(ev)
    if gas_en is None:
        write('gas_en is None\n')
        continue
    gas_en = gas_en.f_11_ENRC()
    
    if gas_en <= gas_min:
        is_ref = True
    else:
        is_ref = False
    
    if pump_on in codes:
        is_pump = True
    else:
        is_pump = False
    
#   save starts here
    if is_pump and not is_ref:
        trace = trace_det.raw_data( ev )
        plt.figure()
        plt.imshow(trace, aspect='auto', cmap=plt.get_cmap("RdBu"))
        plt.title("Timetool Opal Spectral Encoded Image")
        plt.colorbar()
        plt.show()
        #plt.savefig("Opal_WhitelightConvolution.svg",dpi=1200)
        
        if trace is None:
            write('trace is None\n')
            continue
        
        lineout =trace[15:65].mean(0)
        result = TT.analyze(lineout-ref)

        peakpos = result[0]['maxPix']
        peakval = result[0]['max']
        x = peakpos
        time_jitter = pa + pb*x + pc*x*x 
        s = stage_pos(ev) 
        nominal_delay = 2*(s-s0)*1e-3*1e12 /(3*1e8) 
        
        smldata.event( 
            jitter=time_jitter,
            delay=nominal_delay,
            opal_lines= lineout,
            dials= time_dial_det(ev),
            stage = s,  
            opal_peak = result[1], 
            peakval =peakval,
            peakpos = peakpos)

    elif is_pump and is_ref:
        trace = trace_det.raw_data( ev )
        if trace is None:
            write('trace is None\n')
            continue
        ref = trace[15:65].mean(0)

    write('all good\n')
        
smldata.save()
