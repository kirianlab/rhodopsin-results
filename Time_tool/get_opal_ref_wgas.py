###########################
#######
import psana
import argparse
import h5py
import numpy as np


parser = argparse.ArgumentParser(description='Data grab')
parser.add_argument('-r', '--run', type=int, required=True, help='run number to process')
parser.add_argument('-f', '--fname', type=str, default=None, help='output basename')
args = parser.parse_args()

#try:
#    out = h5py.File( args.fname, "w")
#except:
#    print("the file name is missing. No file outputted")
# experiment run number
exp = "cxilu1817"

# average the time tool trace or not
ave_trace=True

# evr codes
pump_on = 183
pump_off = 184
gas_min = 0.5
#probe_off = 162
# 
ds = psana.DataSource("exp=%s:run=%s"%(exp,args.run))
env = ds.env()

# get the detector names for experiment/run
detnames = [ d for sl in psana.DetNames() for d in sl]

# evr codes for laser logic
code_dets = [ psana.Detector(d, env) 
    for d in detnames if d.startswith("evr") ]

#trace
time_dial_det = psana.Detector( "LAS:FS5:VIT:FS_TGT_TIME_DIAL", ds.env() )
trace_det = psana.Detector('Timetool')




events = ds.events()
#pump = []

times_fid = {"pumped":[],"dark":[]}
times_sec = {"pumped":[],"dark":[]}
times_nanosec = {"pumped":[],"dark":[]}
opal_lineouts = {"pumped":[],"dark":[]} 
time_dial = {"pumped":[],"dark":[]} 
shot_counter = {"pumped":0, "dark":0}
seen_evts = 0
gas = psana.Detector("FEEGasDetEnergy")
lineout = {}

import sys
import matplotlib.pyplot as plt
# snatch just the relevant part of back detector (rest is blocked)
for i_ev, ev in enumerate(events):
    sys.stdout.flush()
    seen_evts += 1
    
    if ev is None:
        continue
    trace = trace_det.raw_data( ev )

    if trace is None:
        continue
    
#   parse all event codes
    codes = []
    for det in code_dets:
        c = det.eventCodes(ev)
        if c is not None:
            codes += c
    gas_en = gas.get(ev).f_11_ENRC()
    if gas_en > gas_min:
        #print "\r xrays %d" %i_ev
        print(f"xrays {i_ev}")
        continue
    #if probe_off not in codes:
    #    continue

    if pump_on in codes:
        stat = "pumped"
        shot_counter['pumped'] += 1
    else:
        #print "\r dark continue %d" %i_ev
        print(f"dark continue {i_ev}")
        stat = "dark"
        shot_counter['dark'] += 1

        continue
    print(i_ev)
    lineout['ref'] = trace[15:65].mean(0)
    print(lineout)
    #print ("processed %d pumped and %d dark images out of %d \n" %( shot_counter["pumped"] , shot_counter["dark"] , (i_ev +1 ) ),
    print ("processed %d pumped and %d dark images out of %d \n" %( shot_counter["pumped"] , shot_counter["dark"] , (i_ev +1 ) ))
    break


print("Done")
print(lineout)
import pickle 
#np.savez(args.fname, ref= lineout)
with open(f"{args.fname}", 'wb') as f:
    
    pickle.dump(lineout, f) 

