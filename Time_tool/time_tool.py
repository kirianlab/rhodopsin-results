from numpy.fft import fft, ifft
import numpy as np
import matplotlib.pyplot as plt
#from matplotlib import rc 
#rc('font',**{'family':'sans-serif','sans-serif':['Arial']})
#rc('text')

"""
This script contains functions used in time tool analysis for estimating
time delay

This code runs on PSANA system at LCLS SLAC facility 
"""

class timeToolAnalyzer:
    """
    R kirian code for processing time tool data, we used this
    """
    def __init__(self, my_fit, referenceCount=0, 
        nReferences=100, references=None,
        validReferences=None, meanReference=None,  
        kernel=None, kernelWidth=32, kernelGap=5,
        N=None, visualize=False):

        self.referenceCount = referenceCount
        self.nReferences = nReferences
        self.validReferences = validReferences
        self.references = references
        self.meanReference = meanReference
        self.kernel = kernel
        self.kernelWidth = kernelWidth
        self.kernelGap = kernelGap
        self.N = N
        self.visualize = visualize
        if my_fit is not None:
            self.a, self.b, self.c = my_fit
        else:
            self.a = self.b = self.c = None

    def addReference(self, ref):

        if ref is None:
            return True
        i = self.referenceCount % self.nReferences
        if self.references is None:
            self.N = len(ref)
            self.makeKernel()
            self.validReferences = np.zeros([self.nReferences], dtype=np.int)
            self.references = np.zeros([self.nReferences, self.N])
        self.references[i, :] = ref
        self.validReferences[i] = 1
        self.referenceCount += 1
        return False
    def getMeanReference(self):

        if self.referenceCount == 0:
            return None
        return np.mean(self.references, axis=0)

    def getMeanReferencePowerSpectrum(self):

        return np.mean(np.abs(self.referenceFFTs)**2, axis=0)

    def makeKernel(self):
        # This is essentially a kernel that, when convolved with a signal, will return something that is
        # roughly equivalent to a derivative.
        if self.kernel is not None:
            return
        self.kernel = np.zeros(self.N)
        self.kernel[self.kernelGap:self.kernelWidth] = 1.0 / \
            (self.kernelWidth - self.kernelGap)
        self.kernel += -1 * self.kernel[::-1]
        if self.visualize is True:
            plt.figure()
            plt.plot(self.kernel)
            plt.title('kernel for convolution')
            plt.savefig("kernel.svg",dpi=1200)
            plt.show()
        # Since we use the convolution theorem to take convolutions, we save the FT of the kernel to save time.
        self.kernelftc = np.conjugate(np.fft.fft(self.kernel))

    def pixel2seconds(self, pixel):
        assert(self.A is not None)
        return self.a + self.b * pixel + self.c * pixel**2

    def analyze(self, spec):

        s = spec - np.mean(spec)
        b = self.getMeanReference()
        if b is None:
            b = np.zeros(s.shape)
            # Since we couldn't make the kernel from a reference spectrum...
            self.N = len(s)
            self.makeKernel()
        else:
            b = b.copy()
            b -= np.mean(b)
            b *= np.std(s) / np.std(b)
        # Convolution of the kernel with the background subtracted spectrum
        c = np.real(np.fft.ifft(np.fft.fft(s - b) * self.kernelftc))
        c[0:self.kernelWidth] = 0
        c[-self.kernelWidth:] = 0
        cMaxPix = c.argmax()
        cMax = c.flat[cMaxPix]
        cMean = np.mean(c[self.kernelWidth:-self.kernelWidth])
        cStd = np.std(c[self.kernelWidth:-self.kernelWidth])
        result = {"maxPix": cMaxPix, "max": cMax, "mean": cMean, "std": cStd}
        
        if self.visualize is True:
            
            plt.plot(s)
            print(cMaxPix, cMax)
            #plt.scatter(cMaxPix, cMax, '-o')
            plt.title("trace spectrum ")
            plt.show()
            plt.savefig("trace_minus_mean_trace.svg",dpi=1200)
            plt.figure()
            plt.plot(c)
            plt.title("convolution of the kernel with the background subtracted spectrum")
            plt.savefig("convolution.svg",dpi=1200)
            plt.show()
        return result, c, s, b


